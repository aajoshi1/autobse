function [bseRatio, bseVol] = computeRatioVol(param, inputFilePath,...
    temporaryMaskPath, bseBinPath, erosionSize, diffusionIterations)
% Compute ratio of surface area^1.5 and volume. This is the cost function
% in autobse. params=[edge constant, diffusion constant], inpurFilePath is
% path of the input MRI image. temporarymaskPath is path where temp mask is
% written after computing bse. bseBinPath is the location is bse binary,
% erosionSize and diffusion iterarions are the parameters for bse.

TOO_BIG_VOL = 1800000;
TOO_SMALL_VOL = 800000;
TOO_BIG_RATIO = 20;
TOO_SMALL_RATIO = 10;

edgeConstant = param(1);
if(edgeConstant < 0)
    edgeConstant = 0;
end

diffusionConstant = param(2);% = 25;

tmpMskPth = strrep(temporaryMaskPath, 'tmpAutobseMask', ...
    sprintf('tmpAutobseMask%d_%d', erosionSize, diffusionIterations));

cmdStr = sprintf('%s -i %s --mask %s -d %d -n %d -s %f -r %d', ...
    bseBinPath, inputFilePath, tmpMskPth, diffusionConstant, ...
    diffusionIterations, edgeConstant, erosionSize) ;
[~,~] = dos(cmdStr);

mskOrig = load_nii_BIG_Lab(tmpMskPth);

% 2D operators are used to get rid of brainstem 
% 3D operators for some reason did not show good results

%erosion
originalBW1 = mskOrig.img>0;
se1 = strel('diamond',9);
mskTmp = mskOrig;
mskTmp.img = imerode(originalBW1,se1);

%dilation
se1 = strel('diamond',12);
mskTmp.img = imdilate(mskTmp.img,se1);
bseMsk = mskTmp;
bseMsk.img = and(mskTmp.img,mskOrig.img);

%computing the volume
bseVol = maskVol(bseMsk);

if bseVol==0
    bseRatio=1e6;
else
    %computing the surface area    
    [gx,gy,gz] = gradient(double(bseMsk.img),bseMsk.hdr.dime.pixdim(2),...
        bseMsk.hdr.dime.pixdim(3),bseMsk.hdr.dime.pixdim(4));
    totalArea = sum(sqrt(gx(:).^2+gy(:).^2+gz(:).^2))...
        *bseMsk.hdr.dime.pixdim(2)*bseMsk.hdr.dime.pixdim(3)...
        *bseMsk.hdr.dime.pixdim(4);
    
    %computing the cost function
    bseRatio = ((totalArea)^1.5)/bseVol;
    
    vboundsFlag = (bseVol >= TOO_BIG_VOL) | (bseVol <= TOO_SMALL_VOL);
    rboundsFlag = (bseRatio >= TOO_BIG_RATIO) | (bseRatio <= TOO_SMALL_RATIO);
    bseRatio = (bseRatio + 2*TOO_BIG_RATIO*vboundsFlag ...
        + TOO_BIG_RATIO*rboundsFlag);
    
end
fprintf('.');

