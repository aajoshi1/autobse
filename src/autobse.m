
function autobse(varargin)
% autobse finds optimal parameters of bse and finds the brain mask.
% Usage: 'autobse(bseBin, inputImage, outputmask, MaxIter)' where bseBin is
% the location of bse executable, inputImage is the nifti T1 image,
% outputmask is the location of output mask file, Maxiter(optional) is
% maximum iterations in the optimization process (default 5).

ws=warning;
warning('off');
if nargin <3
    disp('Usage: autobse(bseBin, inputImage, outputmask, MaxIter, flags)');
    disp('Version 2.5');
    disp('bseBin is the location of bse executable,');
    disp('inputImage is the nifti T1 image,');
    disp('outputmask is the location of output mask file,');
    disp(['Maxiter(optional) is max. iterations in the optimization',...
        'process (default 5).']);
    disp('-P (optional) indicates parallel execution enabled. ');
    disp('-R (optional) resample data to 1mm isotropic resolution. ');
end
bseBin=varargin{1}; inputFilePath=varargin{2}; outFile=varargin{3};


parFlag=0;
resFlag=0;
MaxIter=nan;
for jj=4:length(varargin)
    if strfind(varargin{jj},'-P')
        parFlag=1;
        delete(gcp('nocreate'));
        parpool(2);
    end
    if strfind(varargin{jj},'-R')
        resFlag=1;
    end

    if isnan(MaxIter)
        if ischar(varargin{jj})
            MaxIter = str2double(varargin{jj});
        else
            MaxIter = varargin{jj};
        end
    end

end


if isnan(MaxIter)
    MaxIter = 5;
end

fprintf('MaxIter=%d parFlag=%d resFlag=%d\n ',MaxIter, parFlag, resFlag);

workdir = tempname(); mkdir(workdir);

if strcmpi(inputFilePath(end-2:end),'.gz')
    tempFname = fullfile(workdir, [Random_String(16) '.nii.gz']);
    copyfile(inputFilePath, tempFname);
    gunzip(tempFname);
    delete(tempFname);
    inputFilePath = tempFname(1:end-3);
end

if resFlag > 0
    v = load_nii_BIG_Lab(inputFilePath);
 %   opixdim = v.hdr.dime.pixdim(2:4);
     [SX,SY,SZ] = size(v.img);SXo=SX;SYo=SY;SZo=SZ;
     SX = round(SX*v.hdr.dime.pixdim(2));
     SY = round(SY*v.hdr.dime.pixdim(3));
     SZ = round(SZ*v.hdr.dime.pixdim(4));
     v = resample_avw(v,[SX,SY,SZ]);
     save_untouch_nii(v,inputFilePath);
%     %reslice_nii(inputFilePath,inputFilePath,[1,1,1],0,0);
%     
%     v=load_nii_BIG_Lab(inputFilePath);    
%     v.hdr.dime.datatype=4;
%     v.hdr.dime.bitpix=16;
%     save_untouch_nii(v,inputFilePath);

    disp('Input file resampled to 1mm');
end

tmpMskPth = fullfile(workdir,'tmpAutobseMask.nii');
% Do the automated parameter tuning
params = paramTuneBSE(inputFilePath, tmpMskPth, bseBin, MaxIter, parFlag);

disp('The final result is:')
fprintf('diff const = %g,\n diff iter = %d,\n edge const = %g,\n erosion size = %d\n',...
    params.diffusionConstant, params.diffusionIterations,...
    params.edgeConstant, params.erosionSize);

%creating the final output mask file
command = sprintf('%s -i %s --mask %s -d %d -n %d -s %f -r %d --trim -p', bseBin, ...
    inputFilePath, outFile, params.diffusionConstant, ...
    params.diffusionIterations, params.edgeConstant, params.erosionSize);
dos(command);

if resFlag > 0
         v = load_nii_BIG_Lab(outFile);
         v = resample_avw(v,[SXo,SYo,SZo], 'nearest');
         save_untouch_nii_gz(v, outFile);      
end

warning(ws);

%deleting the temporary directory and files
delete([workdir,'/*']);
rmdir(workdir);
