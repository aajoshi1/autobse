function [params] = paramTuneBSE(ifilePath,tmpMskPth,bseBin,MaxIter,parFlag)
% This is the main function that does parameter tuning for autobse.
% ifilePath is the name of the input T1 mri file, tmpMskPth is the location
% of temporary brain mask bseBin is the location of bse binary and MaxIter
% is the number of max iterations in the optimization process.

%find the optimal erosionSize with diffusion iteration fixed as 3
diffIter = 3;
ratio=zeros(2,1);
disp('Searching for Erosion Size');
param=[0,0;0,0];
if parFlag == 0
    for erosionSz = 1:2
        % for erosion size = 1 or 2 and diffusion iteration = 3
        [param(erosionSz,:),ratio(erosionSz)] = fminsearch(@computeRatioVol,...
            [0.64,25], optimset('MaxIter',MaxIter), ifilePath, tmpMskPth, ...
            bseBin, erosionSz, diffIter);
        param(erosionSz,:) = max(0,param(erosionSz,:));
    end    
else
    parfor erosionSz = 1:2
        % for erosion size = 1 or 2 and diffusion iteration = 3
        [param(erosionSz,:),ratio(erosionSz)] = fminsearch(@computeRatioVol,...
            [0.64,25], optimset('MaxIter',MaxIter), ifilePath, tmpMskPth, ...
            bseBin, erosionSz, diffIter);
        param(erosionSz,:) = max(0,param(erosionSz,:));
    end
end

[~,ind] = min(ratio);

fEdgeConst = param(ind,1);
fdiffIter = diffIter;
fdiffConst = param(ind,2);

params.diffusionIterations = fdiffIter;
params.edgeConstant = fEdgeConst;
params.diffusionConstant = fdiffConst;
params.erosionSize = ind;

