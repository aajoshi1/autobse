function vol = maskVol(vm)

vol = sum(vm.img(:)>0) * vm.hdr.dime.pixdim(2) * ...
    vm.hdr.dime.pixdim(3)* vm.hdr.dime.pixdim(4);

