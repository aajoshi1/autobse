% SVReg: Surface-Constrained Volumetric Registration
% Copyright (C) 2016 The Regents of the University of California and the University of Southern California
% Created by Anand A. Joshi, Chitresh Bhushan, David W. Shattuck, Richard M. Leahy
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License
% as published by the Free Software Foundation; version 2.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
% USA.


function [Area] = triArea(faces,verts)

% calculate the areas on all triangles
r12 = verts(faces(:,1),:); % temporary holding
r13 = verts(faces(:,3),:) - r12; % negative of r31
r12 = verts(faces(:,2),:) - r12;            % from 1 to 2

Area = sqrt(sum(cross(r12',r13').^2,1))/2;  % area of each triangle

return

% matlab cross function is much slower, so rewriting
function c = cross(a,b)
c = [a(2,:).*b(3,:)-a(3,:).*b(2,:)
    a(3,:).*b(1,:)-a(1,:).*b(3,:)
    a(1,:).*b(2,:)-a(2,:).*b(1,:)];
return
