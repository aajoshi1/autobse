#!/bin/bash

exe_name=$0
exe_dir=`dirname "$0"`

# If MCR R2015b is installed in a non-default location, define correct path 
# on next line and uncomment it (remove the leading "#")
#BrainSuiteMCR="/path/to/your/MCR";

if [ -z "$BrainSuiteMCR" ]; then
  if [ -e /Applications/MATLAB/MATLAB_Runtime/v90 ]; then
    BrainSuiteMCR="/Applications/MATLAB/MATLAB_Runtime/v90"
  elif [ -e /Applications/MATLAB_R2015b.app/runtime ]; then
    BrainSuiteMCR="/Applications/MATLAB_R2015b.app";  
  else
    echo
    echo "Could not find Matlab 2015b with Matlab Compiler or MCR 2015b (v7.17)."
    echo "Please install the Matlab 2015b MCR from MathWorks at:"
    echo
    echo "http://www.mathworks.com/products/compiler/mcr/"
    echo 
    echo "If you already have Matlab 2015b with the Matlab Compiler or MCR 2015b"
    echo "installed, please edit ${exe_name} by uncommenting and editing the line:"
    echo "#BrainSuiteMCR=\"/path/to/your/MCR\";"
    echo "(replacing /path/to/your/MCR with the path to your Matlab or MCR installation)"
    echo "near the top of the file"
    echo
    exit 78
  fi
fi

read -d '' usage <<EOF

  autobse : Automatic parameter tuning for BSE (autobse)
  Authored by Anand A. Joshi, Gautham Rajagopal, Signal and Image Processing Institute
  Department of Electrical Engineering, Viterbi School of Engineering, USC

  usage: autobse.sh [BSE_executable] [mri_file] [out_mask_file] [optional: maximum iterations] [optional: -P flag]
  Version 2.3

  required input:

  BSE_executable      full path and file name to bse binary in BrainSuite
  mri_file            T1 weighted mri file
  out_mask_file       output mask file
  maximum iterations  maximum number of iterations
  -P (optional flag)  enable parallel execution
  -R (optional flag)  resample data to 1mm isotropic resolution
EOF

# Parse inputs
if [ $# -lt 3 ]; then
  echo
  echo "$usage";
  echo
  exit;
fi

BSE_EXE=$1;
shift

IN_MRI_FILE=$1;
shift

OUT_MASK_FILE=$1;
shift


MAX_ITER=5;
FLAGS=
while [ $# -gt 0 ]; do
  token="$1";
  IS_FLAG=`echo "$token" | grep -q '^-' && echo "T"`;
  
  if [ "$IS_FLAG" = "T" ]; then 
    FLAGS="${FLAGS}${token}";
  else
    MAX_ITER="$token";
  fi
  shift
done



# Set up path for MCR applications.
PATH=${exe_dir}:${PATH} ;
DYLD_LIBRARY_PATH=.:${BrainSuiteMCR}/runtime/maci64 ;
DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:${BrainSuiteMCR}/bin/maci64 ;
DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH}:${BrainSuiteMCR}/sys/os/maci64;
XAPPLRESDIR=${BrainSuiteMCR}/X11/app-defaults ;
export PATH;
export DYLD_LIBRARY_PATH;
export XAPPLRESDIR;

# Run the autobse 
${exe_dir}/autobse.app/Contents/MacOS/autobse "${BSE_EXE}" "${IN_MRI_FILE}" "${OUT_MASK_FILE}" "${MAX_ITER}" "${FLAGS}"

exit
