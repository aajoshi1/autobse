clc;clear all;close all;
%opengl software;
restoredefaultpath;
addpath(genpath('../src'));
fp=fopen('/big_disk/ajoshi/for_Gautham/src/mask_data_full.csv');
names=textscan(fp,'%s');
fclose(fp);
bse='/home/ajoshi/BrainSuite16a1/bin/bse';
jjj=1;
MaxIter=5;
cnt=1;
for jj =1:length(names{1})
    name=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.nii.gz']);
%    maskt=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.auto.iter',num2str(MaxIter),'.mask.nii.gz']);
    maskt=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.auto.iter_v3_',num2str(MaxIter),'.mask.nii.gz']);
    
    if ~exist(maskt,'file')
        continue;
    end
    v=load_nii_BIG_Lab(maskt);
    maskt=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.mask.nii.gz']);
    masktun=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.auto.iter_v2_',num2str(MaxIter),'.mask.nii.gz']);
    if ~exist(masktun,'file')
        continue;
    end
    if ~exist(maskt,'file')
        maskt=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.manual.mask.nii.gz']);
    end

    v1=load_nii_BIG_Lab(maskt);    


    
    DC1(cnt) = DiceCoeff(v.img,v1.img);
    [fp1(cnt),fn1(cnt)] = fposfneg(v1.img, v.img);

    v=load_nii_BIG_Lab(masktun);    
    DC2(cnt) = DiceCoeff(v.img,v1.img);
    [fp2(cnt),fn2(cnt)] = fposfneg(v1.img, v.img);
    
    maskt=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.default.mask.nii.gz']);
    v=load_nii_BIG_Lab(maskt);    
    DC3(cnt) = DiceCoeff(v.img,v1.img);
    [fp3(cnt),fn3(cnt)] = fposfneg(v1.img, v.img);
    

    
    names_new{cnt}=names{1}{jj};
    cnt = cnt+1
end

ind=DC2>0.07 & DC3>0.07;

figure;
plot([DC1(ind);DC2(ind);DC3(ind)]','linewidth',2);

figure;
plot([fp1(ind);fp2(ind);fp3(ind)]','linewidth',2);

figure;
plot([fn1(ind);fn2(ind);fn3(ind)]','linewidth',2);

    
figure;
[hfn1,ed]=histcounts(fn1(:));
[hfn2]=histcounts(fn2(:),ed);
[hfn3]=histcounts(fn3(:),ed);

centers = (ed(1:end-1) + ed(2:end))/2;
figure;
bar(centers, [hfn1',hfn2',hfn3'])
legend('autobse2','autobse','default param');

figure;
[hfp1,ed]=histcounts(fp1(:));
[hfp2]=histcounts(fp2(:),ed);
[hfp3]=histcounts(fp3(:),ed);

centers = (ed(1:end-1) + ed(2:end))/2;
figure;
bar(centers, [hfp1',hfp2',hfp3'])
legend('autobse2','autobse','default param');



