function [DC] = DiceCoeff(img1, img2)
img1=(img1(:)>0);img2=(img2(:)>0);
overlapImage = img2.*img1;
countOverlap=sum(overlapImage);
img1Sz=sum(img1);
img2Sz=sum(img2);
DC = 2*countOverlap/(img1Sz+img2Sz);

