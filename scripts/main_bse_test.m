clc;clear all;close all;
%opengl software;
restoredefaultpath;
addpath(genpath('../src'));
fp=fopen('/big_disk/ajoshi/for_Gautham/src/mask_data_full.csv');
names=textscan(fp,'%s');
fclose(fp);
bse='/home/ajoshi/BrainSuite16a1/bin/bse';
jjj=1;
for MaxIter=[5,10]%[3,5,10,15,35]
    parfor jj =1:length(names{1})
        name=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.nii.gz']);
        %    maskt=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.auto.test2.mask.nii.gz']);
        maskt=fullfile('/big_disk/ajoshi/for_Gautham/BSE_project_2016',names{1}{jj},[names{1}{jj},'.auto.iter_v3_',num2str(MaxIter),'.mask.nii.gz']);
        
%        delete(maskt);
         if ~exist(maskt,'file')
             autobse(bse, name, maskt, num2str(MaxIter));
         end
        jj
    end
end

