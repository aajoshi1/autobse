function [fp,fn] = fposfneg(grndTruth, subMsk)
grndTruth=grndTruth(:);  subMsk=subMsk(:);
fp = sum(grndTruth==0 & subMsk>0)/(sum(grndTruth>0)/2);
fn = sum(grndTruth>0 & subMsk==0)/(sum(grndTruth>0)/2);
