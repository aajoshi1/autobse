# autobse
The program finds best parameters for BSE step in BrainSuite by performing a search.

The method is described in 

Rajagopal, G., Joshi, A.A. and Leahy, R.M., 2017, February. An Algorithm for Automatic Parameter Adjustment for Brain Extraction in BrainSuite. In SPIE Medical Imaging (pp. 101330K-101330K). International Society for Optics and Photonics.

More detailed documentation is available at
https://www.overleaf.com/read/vvthhfpswcrq

Usage: On Linux and Mac autobse.sh bseBin, inputImage, outputmask, MaxIter flag
 bseBin is the location of bse executable
 inputImage is the nifti T1 image
 outputmask is the location of output mask file, Maxiter(optional) is
 maximum iterations in the optimization process (default 5).
 -P optional flag enables parallel execution
 -R flag for resampling to 1mm isotropic before running the bse optimization
